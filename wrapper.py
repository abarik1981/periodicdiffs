"""
!/usr/bin/env python

Program
=======
Periodic Diffs
* This program is used for running a set of NXOS commands periodically
  and then comparing and saving the diff in a log file
* This program uses input as standard cron format
* The diff in presented in unified diff UNIX style and in HTML format
* The HTML report is served via HTTP Server

Usage
=====
Option 1) As Python CLI Script from within NXOS
-----------------------------------------------
# NOTE: You cannot run this fron CLI, must run from bash
NXOS# run bash
NXOS$ python wrapper.py [options]

Option 2) As Python CLI Script from external Linux Machine
----------------------------------------------------------
Linux$ python wrapper.py --external [options]
* NOTE: If username has "\", make sure to provide only 1 "\". e.g: CISCO\myusername

Arguments
=========
python wrapper.py [options]

Options:
  -h, --help           show this help message and exit
  --dir=DIR            Directory where logs will be saved in /<dir>/logs/ and
                       reports will be saved in /<dir>/reports/; accepts full
                       directory path only (platform style); Required
  --cron=CRON          Provide when to collect data as cron argument;
                       Optional. Default: 0 */2 * * *
  --cmdsfile=CMDSFILE  Full path to file with list of commands separated by
                       newline; accepts full directory path only (platform
                       style); Required
  -q                   If True, don't print messages on console; Default:
                       False
  --external           Set this arg if you are running external from NXOS
                       device; Default: False
  --webport=WEBPORT    HTTP Server Port No. to serve the reports in HTML
                       format; Default: 8008


To run the program in background: Do the following
1) Start the script
2) Ctrl+Z
3) bg # Background the process
4) fg # To bring it back

Dependencies
============
* When running from within NXOS, this program depends on NXOS API Python script "cli"
* When running from external Linux Machine, this program depends on NXOS API Python script "nxapi_utils.NXAPITransport"
** This requires Python library lxml
"""

PRGM_TITLE = 'PeriodicDiffs'
PRGM_EPILOG = 'Thanks to Amit Barik!!'
DESCRIPTION_OF_PRGM = '''
Periodic Diffs
* This program will run provided NXOS commands periodically
  and then compare it and then save the diff in a log file

Have fun!!!
'''
WELCOME_NOTE = """Welcome to PeriodicDiffs Program!!"""

DEFAULT_CRON_ARG = "0 */2 * * *"                        # Every 2 hours
DEFAULT_LOGDIR = 'logs'                # Logs folder name (relative to DIR)
DEFAULT_REPORTDIR = 'reports'           # Reports folder name (relative to DIR)
DEFAULT_WEB_PORT = 8008                 # Program will start a HTTP Server at this port

import os
import sys
import socket
import subprocess
from utils.myutils.myoptionparser import MyOptionParser
from utils.myutils.mylogger import Logger

FULLPATH_FILE = os.path.join(os.getcwd(), __file__)
DEFAULT_QUIET = False           # Argument quiet default value
DEFAULT_EXTERNAL = False        # Argument external default value
LOGGER_NAME = 'wrapper'


class Wrapper(object):
    DEFAULT_LOGFILE = 'periodicdiffs.log'                         # Saved under DEFAULT_LOGDIR
    LOGGER_NAME = LOGGER_NAME
    
    def __init__(self, **kwargs):
        self.logger = None
        self.cmdsfile = kwargs.get('cmdsfile')
        self.dir = kwargs.get('dir')
        self.logdir = None
        self.reportdir = None 
        self.quiet = DEFAULT_QUIET
        self.options = None
        self.cron = None
        self.cmdsexe = None
        self.lastrun = ()
        self.logfile = None
        self.webport = DEFAULT_WEB_PORT
        
        # If you are running from External Linux Machine, these properties are required
        self.external = DEFAULT_EXTERNAL
        self.target_url = None
        self.username = None
        self.password = None
        
        # DEFAULT_CCE_CFG_FILE params
        self.cfg_global_params = dict()
    
    def set_arguments(self, **kwargs):
        cmdsfile = kwargs.get('cmdsfile', self.cmdsfile)
        cron = kwargs.get('cron', self.cron)
        _dir = kwargs.get('dir')
        quiet = kwargs.get('quiet', self.quiet)
        external = kwargs.get('external', self.external)
        webport = kwargs.get('webport', self.webport)
        
        self.dir = _dir
        self.logdir = os.path.join(self.dir, DEFAULT_LOGDIR)
        self.reportdir = os.path.join(self.dir, DEFAULT_REPORTDIR)
        
        logs_to_add = list()
        logs_to_add.append(WELCOME_NOTE)
        logs_to_add.append('Arguments provided(default)...')
        logs_to_add.append('cmdsfile:         {0}'.format(repr(cmdsfile)))
        logs_to_add.append('cron:             {0}'.format(repr(cron)))
        logs_to_add.append('logdir:           {0}'.format(repr(self.logdir)))
        logs_to_add.append('reportdir:        {0}'.format(repr(self.reportdir)))
        logs_to_add.append('quiet:            {0}'.format(repr(quiet)))
        logs_to_add.append('webport:          {0}'.format(repr(webport)))
        self.cmdsfile = cmdsfile
        self.cron = cron
        self.quiet = quiet
        self.webport = webport
        
        if not os.path.isfile(self.cmdsfile):
            self.exception("No such file: {0}".format(self.cmdsfile))
            
        if not os.path.isdir(self.dir):
            self.exception("No such directory: {0}".format(self.dir))
        
        if not os.path.isdir(self.logdir):
            logs_to_add.append('Creating new logs dir {0}'.format(self.logdir))
            os.mkdir(self.logdir)
        
        if not os.path.isdir(self.reportdir):
            logs_to_add.append('Creating new reports dir {0}'.format(self.reportdir))
            os.mkdir(self.reportdir)
            
        logfile = os.path.join(self.logdir, self.DEFAULT_LOGFILE)
        if self.logger is None:
            if self.LOGGER_NAME == LOGGER_NAME: # Create file if wrapper is executing it
                self.logger = Logger(name=self.LOGGER_NAME, filename=logfile)
            else:
                self.logger = Logger(name=self.LOGGER_NAME, filename=logfile, filemode='a+')
        self.logfile = logfile
        
        for _logs_to_add in logs_to_add:
            self.log(_logs_to_add, 'INFO')
            
            
        
        self.external = external
        if (self.external is True) and (self.LOGGER_NAME != LOGGER_NAME):
            # Request input only if called from periodicdiff and external arg is set
            msg = ('\n\nYou are running from external Linux device.\n'
                   'You will be asked inputs.\nPlease fill them correctly.\n'
                   'NOTE: Because, you are running from External Linux device,\nits expected that NXOS device'
                   'is running nxapi (conf; feature nxpai)\n')
            
            if self.quiet is True:
                print msg
            self.log(msg, 'INFO')
        
            # Run
            self.target_url = raw_input('Please provide the URL with format: https://<Mgmt IP>/ins: ')
            self.log('target_url: {0}'.format(self.target_url))
            self.username = raw_input('Please provide NXOS device username: ')
            self.log('username: {0}'.format(self.username))
            import getpass
            self.password = getpass.unix_getpass('Please provide NXOS device password: ')
            # Test
#             self.target_url = 'http://10.201.30.194/ins'
#             self.username = r'CISCO\ambarik'
#             self.password = 'xxx'
        
    def parse_arguments(self):
        parser = MyOptionParser()
        parser.title = PRGM_TITLE
        parser.description = DESCRIPTION_OF_PRGM
        parser.epilog = PRGM_EPILOG
        parser.add_option("--dir",
                          dest='dir',
                          help=("Directory where logs will be saved in /<dir>/{0}/ and reports "
                                "will be saved in /<dir>/{1}/; accepts full directory path "
                                "only (platform style); Required".format(DEFAULT_LOGDIR, DEFAULT_REPORTDIR)))
        parser.add_option("--cron",
                          dest='cron',
                          help="Provide when to collect data as cron argument; Optional. Default: {0}".format(DEFAULT_CRON_ARG),
                          default=DEFAULT_CRON_ARG)
        parser.add_option("--cmdsfile",
                          dest='cmdsfile',
                          help="Full path to file with list of commands separated by newline; accepts full directory path only (platform style); Required")
        parser.add_option("-q",
                          dest='quiet',
                          action="store_true",
                          help="If True, don't print messages on console; Default: {0}".format(DEFAULT_QUIET),
                          default=DEFAULT_QUIET)
        parser.add_option("--external",
                          dest='external',
                          action="store_true",
                          help="Set this arg if you are running external from NXOS device; Default: {0}".format(DEFAULT_EXTERNAL),
                          default=DEFAULT_EXTERNAL)
        parser.add_option("--webport",
                          dest='webport',
                          help="HTTP Server Port No. to serve the reports in HTML format; Default: {0}".format(DEFAULT_WEB_PORT),
                          default=DEFAULT_WEB_PORT)
        options = parser.parse_args()[0]
        if not options.dir: # dir is Required
            parser.error('--dir not provided. Use -h to see Help')
            
        self.options = options
        
        _dir = os.path.join(os.getcwd(), options.dir)
        cmdsfile = os.path.join(os.getcwd(), options.cmdsfile)
        cron = options.cron
        quiet = options.quiet
        external = options.external
        webport = options.webport
        self.set_arguments(dir=_dir, cron=cron, cmdsfile=cmdsfile,
                           quiet=quiet, external=external, webport=webport)
        
    
    def log(self, msg, level='DEBUG'):
        if self.logger is None:
            print msg
        else:
            self.logger.getlogger(self.LOGGER_NAME)
            if self.quiet is True:
                self.logger.logger.propagate = False
            self.logger.log(msg, level=level)
    
    def exception(self, reason=None):
        if reason == None and self.logger != None:  # This is called to save the traceback (use it under except clause)
            self.logger.logger.propagate = False
            self.logger.logger.exception("Contact the creator. Time to debug!!")
            raise
        elif reason == None and self.logger == None:
            raise
        else:               # This is called just to log and raise exception, but doesn't save the traceback
            self.log(str(reason), level='WARNING')
            # Close the logfile_prgm
            self.logger = None
            raise Exception(reason)
        
    def run(self):
        mainpkg = os.path.basename(os.path.dirname(FULLPATH_FILE))
        pkg = '{mainpkg}.{modname}'.format(mainpkg=mainpkg, modname='periodicdiffs')
        script_args = {'dir':self.dir,
                       'cmdsfile':self.cmdsfile,
                       'cron':self.cron,
                       'webport': self.webport}
        
        provided_args = self.options.__dict__.copy()
        if 'quiet' in provided_args:
            provided_args.pop('quiet')
        if 'external' in provided_args:
            provided_args.pop('external')
        if len(provided_args.keys()) != len(script_args.keys()):
            reason = 'ImplementationError: Add missing args to script_args manually. Contact Author of this program'
            self.exception(reason)
            
        args = '--dir="{dir}" --cmdsfile="{cmdsfile}" --cron="{cron}" --webport={webport}'.format(**script_args)
        if self.quiet is True:
            args += ' -q'
        if self.external is True:
            args += ' --external'
            
        cmd = 'python -m {pkg} {args}'.format(pkg=pkg, args=args)
        self.log("Running cmd: {0} in background...".format(cmd))
        
        dir_before_mainpkg = os.path.dirname(os.path.dirname(FULLPATH_FILE))
        self.log("dir_before_mainpkg: {0}".format(dir_before_mainpkg))
        ip = socket.gethostbyname(socket.gethostname())
        webport = self.webport
        server_url = 'http://{0}:{1}/'.format(ip, webport)
        msg = """PeriodicDiff Program started in background!!
        
        
        1. This program will run all the commands found in file {cmdsfile}
        2. This program will run these commands periodically as cron values {cron}
        3. This programm will save the output, e.i diff reports, here: {reportdir}
        4. For debugging or looking at logs, see in here: {logdir}
        5. Webserver started!! Please navigate to this link: {server_url}
        
        Thank you for using this program. For any questions, please contact
        Amit Barik at ambarik@cisco.com
        
        """.format(reportdir=self.reportdir, cron=self.cron,
                   cmdsfile=self.cmdsfile, logdir=self.logdir,
                   server_url=server_url)
        if self.quiet is True:
            print msg
        self.log(msg, 'INFO')
        p = subprocess.Popen(cmd, shell=True, cwd=dir_before_mainpkg, stdin=sys.stdin)
        msg = """
        To kill this program (with PID: {pid}), do the following:
                hostname# run bash
                hostname$ kill -9 {pid}
        """.format(pid=p.pid)
        with open(self.logfile, 'a+') as _file:
            # Manually adding, as Logger has been taken over by other script
            _file.write(msg)
        print msg
        p.wait()
        

def test():
    from utils.extutils.croniter.croniter import croniter
    from datetime import datetime
    base = datetime(2010, 1, 25, 4, 46)
    iterme = croniter('*/5 * * * *', base)  # every 5 minites
    print iterme.get_next(datetime)   # 2010-01-25 04:50:00
    print iterme.get_next(datetime)   # 2010-01-25 04:55:00
    sys.exit()
    
def main():
    #test()
    
    # Initialize
    wrapper = Wrapper()
    try:
        wrapper.parse_arguments()
        wrapper.run()
    except:
        wrapper.exception()
    
if __name__ == '__main__':
    main()

