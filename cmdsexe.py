'''
@summary: This is used for running commands on local device using Python API of device and 
          saving the output in an ini file
          with format: 
          cmd :-> 'output'
@author: ambarik@cisco.com
'''

DEFAULT_CMDS_FILE = 'cmdsfile.ini'
CFG_OUT = r'{filename}-Out{ext}'

import os

try:
    from cli import cli     # NXOS Internal API
    IS_EXTERNAL = False
except ImportError:         # NXOS External API
    from utils.extutils.nxapi.nxapi_utils import NXAPITransport
    cli = NXAPITransport.cli
    IS_EXTERNAL = True
    
from utils.myutils.myconfigparser import MyConfigParser
from utils.myutils.mylogger import Logger
from wrapper import Wrapper
from wrapper import DEFAULT_QUIET, DEFAULT_EXTERNAL

class CmdsExe(Wrapper):
    LOGGER_NAME = 'cmdsexe'
    DEFAULT_LOGFILE = 'cmdsexe.log'                         # Saved under DEFAULT_LOGDIR
    
    def __init__(self, **kwargs):
        self.logger = None
        self.dir = kwargs.get('dir')
        self.logfile_prgm = kwargs.get('logfile_prgm', self.DEFAULT_LOGFILE)
        self.cmdsfile = kwargs.get('cmdsfile', DEFAULT_CMDS_FILE)
        self.cfgoutfile = kwargs.get('cfgoutfile')
        self.configout = MyConfigParser()
        self.configout.optionxform = str
        self.quiet = kwargs.get('quiet', DEFAULT_QUIET)
        
        # If you are running from External Linux Machine, these properties are required
        self.external = kwargs.get('external', DEFAULT_EXTERNAL)
        self.target_url = kwargs.get('target_url')
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        

        # DEFAULT_CCE_CFG_FILE params
        self.cfg_global_params = dict()
        
        self.set_arguments()
        
        # Host properties
        self.hostname = None
        
    def set_arguments(self, **kwargs):
        """Uses parsed/provided arguments to:
        * create directory/assign directory
        * define directory path
        """
        _dir = kwargs.get('dir', self.dir)
        logfile_prgm = kwargs.get('logfile_prgm', self.logfile_prgm)
        cmdsfile = kwargs.get('cmdsfile', self.cmdsfile)
        
        logs_to_add = list()
        logs_to_add_debug = list()
        logs_to_add.append('Arguments provided(default)...')
        logs_to_add.append('dir:                  {0}'.format(repr(_dir)))
        logs_to_add.append('logfile_prgm:         {0}'.format(repr(logfile_prgm)))
        logs_to_add.append('cmdsfile:         {0}'.format(repr(cmdsfile)))
        
        if _dir is None:
            self.exception("Argument 'dir' not specified")
        self.dir = os.path.abspath(_dir)
        if not os.path.isdir(self.dir):
            self.exception("No such directory: {0}".format(self.dir))
        
        if not os.path.isfile(logfile_prgm):
            # file doesn't exist
            if os.path.isdir(os.path.dirname(logfile_prgm)) is False:
                # dir doesn't exists or not provided; use only filename
                self.logfile_prgm = os.path.join(self.dir, os.path.basename(logfile_prgm))
                logs_to_add_debug.append("logfile_prgm's basename is being used only with dir argument as directory path")
            else:
                # dir exists; use full path
                self.logfile_prgm = logfile_prgm
                logs_to_add_debug.append("Using logfile_prgm's directory path and filename. Not using dir argument here")
        else:
            # file exists; use full path
            logs_to_add_debug.append("Using existing logfile_prgm: {0}".format(logfile_prgm))
            self.logfile_prgm = logfile_prgm
        
        self.logger = Logger(name=self.LOGGER_NAME, filename=self.logfile_prgm, filemode='a+')
            
        for _logs_to_add_debug in logs_to_add_debug:
            self.log(_logs_to_add_debug)
        for _logs_to_add in logs_to_add:
            self.log(_logs_to_add, 'INFO')
        
        if not os.path.isfile(cmdsfile):
            self.log("Not using directory from {0} as filename doesn't exist. Using dir argument with basename".format(cmdsfile))
            # file doesn't exist; use filename only
            self.cmdsfile = os.path.join(self.dir, os.path.basename(cmdsfile))
        else:
            # file exists; use full path
            self.log("Using logfile_prgm: {0}".format(cmdsfile))
            self.cmdsfile = cmdsfile
            
        filename, ext = os.path.splitext(os.path.basename(self.cmdsfile))
        self.cfgoutfile = CFG_OUT.format(filename=filename, ext=ext)
        self.cfgoutfile = os.path.join(self.dir, self.cfgoutfile)
        
    def run(self):
        if self.external != IS_EXTERNAL:
            reason = 'Program incorrectly used!! --external argument must be provided if you are running from external Linux Server'
            self.exception(reason)
            
        if self.external is True:
            # Running from External Linux Machine
            if None in [self.target_url, self.username, self.password]:
                reason = 'Username, password or target_url is not provided!!'
                self.exception(reason)
            NXAPITransport.init(target_url=self.target_url, username=self.username, password=self.password)
            
        self.hostname = cli('show hostname').strip()
        self.log('Executing cmds for hostname: {0}'.format(self.hostname))
        self.configout.add_section(self.hostname)
        
        for cmd in open(self.cmdsfile):
            if '#' == cmd[0]:
                continue    # Skip any comments
            cmd = cmd.rstrip('\n')
            self.log('Executing cmd: {0}'.format(cmd))
            cmd_output = cli(cmd)
            self.configout.set(self.hostname, cmd, repr(cmd_output))
        
        self.create_cfgout()
    
    def create_cfgout(self):
        with open(self.cfgoutfile, 'wb') as configfile:
            self.configout.write(configfile)
        self.log('Created Config Out file: {0}'.format(self.cfgoutfile), 'INFO')
        
    
def main():
    import getpass
    cmdsexe = CmdsExe(dir='/tmp/periodicdiffs/logs/20140709214341', cmdsfile='/tmp/periodicdiffs/cmds.ini',
                      quiet=False, external=True,
                      target_url="https://10.201.30.194/ins", username="CISCO\\ambarik",
                      password=getpass.getpass())
    cmdsexe.run()

if __name__ == '__main__':
    main()
    