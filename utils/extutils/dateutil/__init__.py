"""
This is a local copy of the package to support croniter
(as I couldn't install packages on the switch)

@author: https://pypi.python.org/pypi/python-dateutil
@change: Modified file dateutil.relativedelta and 
         __author__ to avoid ASCII error
"""
# -*- coding: utf-8 -*-
"""
Copyright (c) 2003-2010  Gustavo Niemeyer <gustavo@niemeyer.net>

This module offers extensions to the standard Python
datetime module.
"""
__author__ = "Tomi Pievilainen <tomi.pievilainen@iki.fi>"
__license__ = "Simplified BSD"
__version__ = "2.2"
