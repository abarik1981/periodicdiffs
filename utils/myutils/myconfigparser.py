'''
@summary: This module overrides ConfigParser of Python2.6 to work it like Python 2.7
          and for my needs
@author: ambarik@cisco.com
'''

import sys
from myordereddict import MyOrderedDict
from ConfigParser import ConfigParser
from ConfigParser import RawConfigParser
from ConfigParser import DEFAULTSECT
from ConfigParser import MissingSectionHeaderError
from ConfigParser import NoOptionError
import re


class MyConfigParser(ConfigParser):
    # Copy of ConfigParser from Python 2.7
    SEP = ':->'
    OPTCRE = re.compile(
        r'(?P<option>.*[^{sep}])'         # Option - (will not contain the separator)
        r'\s*(?P<vi>{sep})\s*'               # Separator - any number of space/tab, followed by separator, followed by any # space/tab
        r'(?P<value>.*)$'.format(sep=SEP)     # Value - everything up to eol
        )
    OPTCRE_NV = OPTCRE

    def __init__(self, **kwargs):
        '''
        Overriding to support for no valuev
        '''
        defaults = kwargs.get('defaults')
        dict_type = kwargs.get('dict_type', MyOrderedDict)
        allow_no_value = kwargs.get('allow_no_value', True)

        if '2.7' in str(sys.version):
            RawConfigParser.__init__(self, defaults=defaults, dict_type=dict_type,
                 allow_no_value=allow_no_value)
        else:
            RawConfigParser.__init__(self, defaults=defaults, dict_type=dict_type)
            # Support for no value
            if allow_no_value is True:
                self._optcre = self.OPTCRE_NV
            else:
                self._optcre = self.OPTCRE

    def optionxform(self, optionstr):
        '''
        Overriding to NOT make option lower
        '''
        return optionstr

    def set(self, section, option, value=None):
        '''
        Overriding to make value default to None
        '''
        ConfigParser.set(self, section, option, value)

    def get(self, section, option):
        '''
        Overriding to return None by default
        
        Args:
        section, option, raw=False, vars=None
        '''
        
        try:
            return str(self._sections[section][option])
        except KeyError:
            return None

    def write(self, fp):
        """Overriding: Copy from Python 2.7
        Write an .ini-format representation of the configuration state."""
        if self._defaults:
            fp.write("[%s]\n" % DEFAULTSECT)
            for (key, value) in self._defaults.items():
                write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, val=str(value).replace('\n', '\n\t'))
                fp.write(write_line)
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key != "__name__":
                    if value is None:
                        fp.write("%s\n" % (key))
                    else:
                        write_line = "{key} {sep} {val}\n".format(key=key, sep=self.SEP, val=str(value).replace('\n', '\n\t'))
                        fp.write(write_line)
            fp.write("\n")

    def _read(self, fp, fpname):
        """Overriding: Copy from Python 2.7"""
        cursect = None                            # None, or a dictionary
        optname = None
        lineno = 0
        e = None                                  # None, or an exception
        while True:
            line = fp.readline()
            if not line:
                break
            lineno = lineno + 1
            # comment or blank line?
            if line.strip() == '' or line[0] in '#;':
                continue
            if line.split(None, 1)[0].lower() == 'rem' and line[0] in "rR":
                # no leading whitespace
                continue

            # is it a section header?
            mo = self.SECTCRE.match(line)
            if mo:
                sectname = mo.group('header')
                if sectname in self._sections:
                    cursect = self._sections[sectname]
                elif sectname == DEFAULTSECT:
                    cursect = self._defaults
                else:
                    cursect = self._dict()
                    cursect['__name__'] = sectname
                    self._sections[sectname] = cursect
                # So sections can't start with a continuation line
                optname = None
            # no section header in the file?
            elif cursect is None:
                raise MissingSectionHeaderError(fpname, lineno, line)
            # an option line?
            else:
                mo = self._optcre.match(line)
                if mo:
                    optname, vi, optval = mo.group('option', 'vi', 'value')
                    # This check is fine because the OPTCRE cannot
                    # match if it would set optval to None
                    if optval is not None:
                        if str(vi) in self.SEP and ';' in optval:
                            # ';' is a comment delimiter only if it follows
                            # a spacing character
                            pos = optval.find(';')
                            if pos != -1 and optval[pos-1].isspace():
                                optval = optval[:pos]
                        optval = optval.strip()
                    optname = optname.rstrip()
                    cursect[optname] = optval
                else:
                    optval = None
                    optname = line.rstrip()
                    cursect[optname] = optval


def main():
    #test
    #cfg_file = r'C:\Barik\eclipse_workspace\cygwin_project\CCA\Test\CiscoVPNComcastSELab-Out.cfg'
    cfg_file = r'C:\Barik\MyPythonCCECygwin\CCE\test\CCE-Out.cfg'
#    config = MyConfigParser()
#    section_name1 = 'Device1'
#    cmd1 = '    show run1'
#    config.add_section(section_name1)
#    config.set(section_name1, cmd1)
#    section_name2 = 'Device2'
#    cmd2 = 'cmd1'
#    config.add_section(section_name2)
#    config.set(section_name2, cmd2, 'test2')
#    
#    print config.get('Device2', 'cmd2')
#    #print config.get('Device2', 'asdasdas')
#    config.set('Device2', 'cmd2', 'test2')
#    config.set('Device2', 'cmd2', 'test2')
#    print config._sections
#    with open(cfg_file, 'a') as configfile:
#        config.write(configfile)
#    
    
    config = MyConfigParser()
    config.readfp(open(cfg_file))
    out1 = eval(config._sections['172.18.87.121']['show alarms1'])
    out2 =eval(config._sections['172.18.87.121']['show alarms2'])
    import difflib
    diffSequence = difflib.unified_diff(out1.splitlines(1), out2.splitlines(1), fromfile="timestamp1", tofile="timestamp2", n=1)
    diff = list()
    for i, line in enumerate(diffSequence):
        diff.append(line)
        #print line,
    diff = ''.join(diff)
    print 'diff', diff
    #print config._sections

if __name__ == '__main__':
    main()

