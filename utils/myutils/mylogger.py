'''
@summary: This module creates a Logger for my needs
          # http://stackoverflow.com/questions/13210436/get-full-traceback
@author: ambarik@cisco.com
'''

DEFAULT_FILENAME = 'test.log'
import sys, os
import logging
from logging import StreamHandler, BASIC_FORMAT, Formatter, root, codecs

class FileHandler(logging.FileHandler):
    """
    Overridden to not use self.baseFilename
    """
    def __init__(self, filename, mode='a', encoding=None, delay=0):
        self.__filename__ = filename
        logging.FileHandler.__init__(self, filename, mode, encoding, delay)

    def _open(self):
        """
        Copy/Paste of FileHandler._open
        Changed - self.__filename__
        """
        if self.encoding is None:
            stream = open(self.__filename__, self.mode)
        else:
            stream = codecs.open(self.__filename__, self.mode, self.encoding)
        return stream

class Logger():
    def __init__(self, **kwargs):
        self.logger = None
        self.name = kwargs.get('name', 'root')
        self.filename = kwargs.get('filename', DEFAULT_FILENAME)
        self.filemode = kwargs.get('filemode', 'w')
        self.level = kwargs.get('level', logging.DEBUG)
        self.level_console = kwargs.get('level_console', logging.INFO)
        self.format_logger = kwargs.get('format_logger', '%(asctime)s %(name)-10s %(threadName)-10s %(levelname)-8s %(message)s')
        self.datefmt = kwargs.get('datefmt', '%b %d %H:%M:%S')
        
        self.setup(**kwargs)
        msg = 'Logger logs are saved in {0}'.format(self.filename)
        self.log(msg)
    
    def __del__(self):
        for hdlr in self.logger.handlers:
            self.logger.removeHandler(hdlr)
            hdlr.flush()
            hdlr.close()
            
    def setup_rootlogger(self, **kwargs):
        """
        Copy/Paste of logging.basicConfig
        """

        if len(root.handlers) == 0:
            filename = kwargs.get("filename", self.filename)
            if filename:
                mode = kwargs.get("filemode", self.filemode)
                hdlr = FileHandler(filename, mode)
            else:
                stream = kwargs.get("stream")
                hdlr = StreamHandler(stream)
            
            fs = kwargs.get("format_logger", self.format_logger)
            dfs = kwargs.get("datefmt", self.datefmt)
            fmt = Formatter(fs, dfs)
            hdlr.setFormatter(fmt)
            root.addHandler(hdlr)
            level = kwargs.get("level", self.level)
            if level is not None:
                root.setLevel(level)
            
            
            # set a format which is simpler for console use
            console = logging.StreamHandler()
            console.setLevel(self.level_console)
            console_formatter = logging.Formatter(kwargs.get('format_console', '%(name)-10s: %(levelname)-8s %(message)s'))
            console.setFormatter(console_formatter)
            self.logger = logging.getLogger('')
            self.logger.addHandler(console)
        
    def setup(self, **kwargs):
        # set up logging to file
        self.setup_rootlogger(**kwargs)
        self.logger = logging.getLogger(self.name)
        hdlr = FileHandler(self.filename, self.filemode)
        hdlr.setFormatter(Formatter(self.format_logger, self.datefmt))
        self.logger.addHandler(hdlr)
    
    def getlogger(self, name):
        self.logger = logging.getLogger(name)
        
    def log(self, msg, level='INFO'):
        if self.logger is None:
            self.logger = logging.getLogger('')
        if level == 'INFO':
            self.logger.info(msg)
        elif level == 'WARNING':
            self.logger.warning(msg)
        elif level == 'DEBUG':
            self.logger.debug(msg)
        elif level == 'ERROR':
            self.logger.error(msg)
        elif level == 'EXCEPTION':
            self.logger.exception(msg)


class test(object):
    
    def __init__(self, log):
        self.log = log
        
    def test(self):
        #print xyz
        try:
            print x
        except Exception as e:
            self.log.log(msg='bad', level='EXCEPTION')
            print e
        print '234'
        self.log.log(msg='nothing', level='EXCEPTION')
        
def main():
    mylogger = Logger(filename='root.log')
    mylogger.log('helloroot')
    
    mylogger = Logger(name='logger1', filename='x1.log')
    mylogger.log('hellologger1', 'DEBUG')
    
    mylogger = Logger(name='logger2', filename='x2.log')
    mylogger.log('hellologger2', 'DEBUG')
    
    mylogger.getlogger('logger1')
    mylogger.log('hellologger11', 'DEBUG')
    
    mylogger.getlogger('logger1part1')
    mylogger.log('hellologger111', 'DEBUG')
    
    #t = test(log)
    #t.test()

if __name__ == '__main__':
    main()
