'''
@summary: This module overrides OptionParser for my needs 
@author: ambarik@cisco.com
'''

from optparse import OptionParser

class MyOptionParser(OptionParser):
    def format_description(self, formatter):
        '''
        Override to avoid adding newline. Just return the description
        '''
        return self.description