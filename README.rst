============
PeriodicDiff
============

Program
=======
Periodic Diffs

- This program is used for running a set of NXOS commands periodically
  and then comparing and saving the diff in a log file
  
- This program uses input as standard cron format

- The diff in presented in UNIX style

- The HTML report is served via HTTP Server


Usage
=====
1) Option: As Python CLI Script from within NXOS
-----------------------------------------------

CLI snippet::

	# NOTE: You cannot run this fron CLI, must run from bash
	NXOS# run bash
	NXOS$ python wrapper.py [options]


2) Option: As Python CLI Script from external Linux Machine
----------------------------------------------------------
Linux$ python wrapper.py --external [options]

- NOTE: :code:`If username has "\", make sure to provide only 1 "\". e.g: CISCO\myusername`


Arguments
=========
python wrapper.py [options]

Options:
  -h, --help           show this help message and exit
  --dir=DIR            Directory where logs will be saved in /<dir>/logs/ and
                       reports will be saved in /<dir>/reports/; accepts full
                       directory path only (platform style); Required
  --cron=CRON          Provide when to collect data as cron argument;
                       Optional. Default: 0 */2 * * *
  --cmdsfile=CMDSFILE  Full path to file with list of commands separated by
                       newline; accepts full directory path only (platform
                       style); Required
  -q                   If True, don't print messages on console; Default:
                       False
  --external           Set this arg if you are running external from NXOS
                       device; Default: False
  --webport=WEBPORT    HTTP Server Port No. to serve the reports in HTML
                       format; Default: 8008
                       

To run the program in background: Do the following

1) Start the script

2) Ctrl+Z

3) bg # Background the process

4) fg # To bring it back


Dependencies
============
- When running from within NXOS, this program depends on NXOS API Python script "cli"

- When running from external Linux Machine, this program depends on NXOS API Python script "nxapi_utils.NXAPITransport"

	- This requires Python library lxml

- Please go through ./examples/EXAMPLE.rst that documents the setup steps
 