'''
#! /usr/bin/env python

Program
=======
Periodic Diffs
Refer wrapper.py

Usage
=====
Option 1) As Python module
--------------------------

Option 2) As Python CLI Script
------------------------------
Refer wrapper.py for arguments

'''

import os
import difflib
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
from utils.myutils.mydifflib import UnifiedDiffer
from cmdsexe import CmdsExe
from utils.myutils.myconfigparser import MyConfigParser
from utils.extutils.BeautifulSoup import BeautifulSoup


# Default Constants
DEFAULT_REPORT_HTML_FILE = 'periodicdiffs.html'           # Saved under DEFAULT_REPORT_DIR
DEFAULT_REPORT_TXT_FILE = 'periodicdiffs.txt'           # Saved under DEFAULT_REPORT_DIR

DIR_FORMAT = "%Y%m%d%H%M%S"
PRETTY_TIMESTAMP_FORMAT = '%d-%b-%Y %H:%M:%S %Z'
DEFAULT_OOP_SEP = '~.~'                                 # Separator used in Report Config file
PATTERN_DONT_DIFF = '^report-|^CCE|^!|^sh\w* log|^DynamicCode|^terminal|^telnet|^__name__'
WELCOME_NOTE = """# Periodicdiff.txt report\n
# @author: Amit Barik\n
# Created at {dt} (Timestamps are wrt periodicdiffs Server Timezone)\n
# This file contains all the diffs b/w cron-runs reported by periodicdiffs program in {ff}\n
# Running with cron-job: '{cj}'\n
# Commands running on hostname: '{hostname}'\n
# The diff in b/w outputs of current vs previous cron-job is shown in Unix diff unified format\n
# Output of commands are found in file {ts}, where '%timestamp%' is the current or previous timestamp\n"""
NUM_LINES_CONTEXT_TXT = 0
NUM_LINES_CONTEXT_HTML = 5

import re
import time
from datetime import datetime
from utils.extutils.croniter.croniter import croniter
from wrapper import Wrapper
    
class PeriodicDiffs(Wrapper):
    LOGGER_NAME = "periodicdiffs"
    
    def __init__(self, **kwargs):
        super(PeriodicDiffs, self).__init__()
        
        self.logger = None
    
    def run(self, **kwargs):
        # Running server
        import threading
        t = threading.Thread(target=self.serve_website)
        t.daemon = True
        t.start()
        
        # Run data collector and differ
        dir_format = DIR_FORMAT
        i = 1
        current_time = datetime.now()
        cron_iter = croniter(self.cron, current_time)
        while True:
            self.log("Running data collect at {0} with iterator {1}".format(current_time, i), 'INFO')
            current_dir = os.path.join(self.logdir, current_time.strftime(dir_format))
            
            self.log("Creating directory: {0}".format(current_dir), 'INFO')
            os.mkdir(current_dir)
            
            # Run wrapper
            self.cmdsexe = CmdsExe(dir=current_dir, cmdsfile=self.cmdsfile,
                                   quiet=self.quiet, external=self.external,
                                   target_url=self.target_url, username=self.username,
                                   password=self.password)
            self.cmdsexe.run()
            
            # Generate report
            if i == 1:  # 1st run
                self.generate_reports(self.cmdsexe.cfgoutfile, current_time, first_run=True)
            else:
                self.generate_reports(self.cmdsexe.cfgoutfile, current_time)
                
            # Wait for next_time
            next_time = cron_iter.get_next(datetime)
            td = (next_time - datetime.now())
            sleep_time = (td.microseconds + (td.seconds + td.days * 24.00 * 3600) * 10**6) / 10**6 # No total_seconds() in 2.6
            msg = 'Waiting for {0} minutes'.format(sleep_time/60)
            self.log(msg, 'INFO')
            while next_time > datetime.now():
                self.log("Sleeping for {0} seconds".format(sleep_time/4))
                time.sleep(sleep_time/4)
                
            current_time = datetime.now()
            i += 1
    
    def generate_reports(self, current_cfgout_file, current_datetime, first_run=False):
        current_timestamp = current_datetime.strftime(DIR_FORMAT)
        self.log("Generating Reports for run with timestamp: {0}".format(current_timestamp), 'INFO')
        report_txt_file = os.path.join(self.reportdir, DEFAULT_REPORT_TXT_FILE)
        if self.target_url is None:
            hostname = 'localhost'
        else:
            hostname = self.target_url.replace('http://', '').replace('https://', '').replace('/ins', '')
        if not os.path.exists(report_txt_file):
            self.log("Creating report config file: {0}".format(report_txt_file), 'INFO')
            ts = current_cfgout_file.replace(current_timestamp, '%timestamp%')
            file_format = 'plain text format'
            welcome_note = WELCOME_NOTE.format(dt=datetime.now(),
                                ff=file_format,
                                cj=self.cron,
                                ts=ts,
                                hostname=hostname)
            with open(report_txt_file, 'w') as _file:
                _file.write(welcome_note)
                _file.close()
        
        report_html_file = os.path.join(self.reportdir, DEFAULT_REPORT_HTML_FILE)
        diff_table_id = re.compile('difflib_')  # To match id seen in <table id="difflib_foo">
        if not os.path.exists(report_html_file):
            self.log("Creating report config file: {0}".format(report_html_file), 'INFO')
            ts = current_cfgout_file.replace(current_timestamp, '%timestamp%')
            file_format = 'HTML format'
            welcome_note = WELCOME_NOTE.format(dt=datetime.now(),
                                ff=file_format,
                                cj=self.cron,
                                ts=ts,
                                hostname=hostname).replace('\n', '</li>').replace('# ', '<li>')
            welcome_note = '<h1>PeriodicDiffs</h1><ul>{0}</ul></h2><ul id="root" class="root"></ul>'.format(welcome_note)
            template_diff_html = difflib.HtmlDiff().make_file([], [])
            template_soup = BeautifulSoup(template_diff_html)
            template_soup.find('table', id=diff_table_id).replaceWith(BeautifulSoup(welcome_note))
            template_soup.find('title').string = 'PeriodicDiffs'
            with open(report_html_file, 'w') as _file:
                _file.write(template_soup.renderContents())
                _file.close()
                
        # Generating Report Config File
        report_txt = list()
        txt_add_format = '    :    {ts}    :     '.format(ts=datetime.now())
        
        # Parsing Current Timestamped Output Config File
        current_cfgout = MyConfigParser()
        current_cfgout.readfp(open(current_cfgout_file))
        current_cfgout = current_cfgout._sections.copy()
        # Backward compatible with CCE
        if current_cfgout.get('ERRORS') is not None:
            errors =  current_cfgout.pop('ERRORS')
            failed_device_list = eval(errors['Failed device list'])
            if len(failed_device_list) != 0:
                for dev in failed_device_list:
                    report_txt.append('# ERROR' + txt_add_format + 'Failed device: {0} with reason: {1}'.format(dev, repr(errors[dev])))
                
        # Save error reports
        for hostname in current_cfgout:
            for error_key in current_cfgout[hostname]:
                if 'report-' in error_key:
                    report_txt.append('# WARNING' + txt_add_format + '{0}'.format(error_key.lstrip('report-')))
                    
        # If not first time run, then compare and save diff with previous run
        if first_run is False:
            lastrun_cfgout_file = self.lastrun[0]
            lastrun_timestamp = self.lastrun[1].strftime(DIR_FORMAT)
            lastrun_cfgout = MyConfigParser()
            lastrun_cfgout.readfp(open(lastrun_cfgout_file))
            lastrun_cfgout = lastrun_cfgout._sections.copy()
            soup = BeautifulSoup(open(report_html_file).read())
            li_root_tag = BeautifulSoup('<li id="{0}"></li>'.format(current_timestamp))
            for hostname in current_cfgout:
                ul_subroot_tag = BeautifulSoup('<ul class="subroot"></ul>')
                for cmd_to_diff in current_cfgout[hostname]:
                    if re.search(PATTERN_DONT_DIFF, cmd_to_diff) is not None:
                        continue
                    
                    current_cmd_out = current_cfgout[hostname][cmd_to_diff]
                    lastrun_cmd_out = lastrun_cfgout[hostname][cmd_to_diff]
                    
                    try:
                        run1lines = eval(current_cmd_out).splitlines(1)
                    except (SyntaxError, NameError):
                        run1lines = str(current_cmd_out).splitlines(1)
                    try:
                        run2lines = eval(lastrun_cmd_out).splitlines(1)
                    except (SyntaxError, NameError):
                        run2lines = str(lastrun_cmd_out).splitlines(1)
                    
                    ## If your platform spits out timestamp for every show command you wish to ignore it, uncomment this (e.g in IOSXR)
                    #platform_timestamp_pattern = '^([a-zA-Z]{3}\s+){2}\d+\s+(\d{2}:){2}\d{2}\.\d+\s+[A-Z]{3}\s+$'
                    #if re.search(platform_timestamp_pattern, run1lines[:1][0]) is not None and re.search(platform_timestamp_pattern, run2lines[:1][0]) is not None:
                    #    # Ignore Timestamp
                    #    run1lines = run1lines[1:]
                    #    run2lines = run2lines[1:]
                    
                    # Diff for txt output
                    fromfile = tofile = "Command output: '{0}' at".format(cmd_to_diff)
                    fromfiledate = "{0}".format(current_datetime.strftime(PRETTY_TIMESTAMP_FORMAT))
                    tofiledate = "{0}".format(self.lastrun[1].strftime(PRETTY_TIMESTAMP_FORMAT))
                    diff_unified_txt = ''.join(UnifiedDiffer().unified_diff(run1lines, run2lines,
                                                                fromfile=fromfile, 
                                                                tofile=tofile,
                                                                fromfiledate=fromfiledate,
                                                                tofiledate=tofiledate,
                                                                n=NUM_LINES_CONTEXT_TXT))
                    style = '#'*5
                    msg_per_cmd = '#{1} Diff for output of command "{0}" {1}'.format(cmd_to_diff, style)
                    report_txt.append(msg_per_cmd)
                    report_txt.append(diff_unified_txt)
                    diff_unified_html = difflib.HtmlDiff().make_file(run1lines, run2lines,
                                                                     fromfile + ' ' + fromfiledate, 
                                                                     tofile + ' ' + tofiledate,
                                                                     context=True,
                                                                     numlines=NUM_LINES_CONTEXT_HTML)
                    diff_table_soup = BeautifulSoup(diff_unified_html).find('table', id=diff_table_id)
                    li_subroot_tag = BeautifulSoup('<li></li>')
                    li_subroot_tag.find('li').insert(0, diff_table_soup)
                    li_subroot_tag.find('li').insert(0, BeautifulSoup('<h4>{0}</h4>'.format(msg_per_cmd.strip('#').strip())))
                    ul_subroot_tag.find('ul').append(li_subroot_tag)
                li_root_tag.find('li').append(ul_subroot_tag)
            
            self.log("Appending data gathered from run with timestamp {0} to report text file".format(current_timestamp), 'INFO')
            msg_per_run = '#{2} Diff b/w CurrentTimestamp=({0}) Vs '\
                            'PreviousTimestamp=({1}) {2}\n'.format(current_timestamp, 
                                                                     lastrun_timestamp, 
                                                                     style)
            with open(report_txt_file, 'a') as _file:
                style = '#'*10
                _file.write('\n\n\n' + style + '\n')
                _file.write(msg_per_run)
                _file.write(style + '\n')
                _file.write('\n'.join(report_txt))
            
            self.log("Modifying HTML for timestamp {0}".format(current_timestamp), 'INFO')
            with open(report_html_file, 'w') as _file:
                li_root_tag.find('li').insert(0, BeautifulSoup('<h3>{0}</h3>'.format(msg_per_run.strip().strip('#').strip())))
                soup.find('ul', id='root').append(li_root_tag)
                style = soup.find('head').find('style')
                style.append('ul.root, ul.subroot {list-style-type: none; padding-left: 0px;}\n')
                style.append('h3 {color: blue;}\n h4 {color: crimson; text-decoration: underline;}\n')
                _file.write(soup.renderContents())
                
        self.lastrun = (current_cfgout_file, current_datetime)
        
    def serve_website(self):
        os.chdir(self.reportdir)
        HandlerClass = SimpleHTTPRequestHandler
        Protocol     = "HTTP/1.0"
        HandlerClass.protocol_version = Protocol
        ServerClass  = BaseHTTPServer.HTTPServer
        httpd = ServerClass(('0.0.0.0', int(self.webport)), HandlerClass)
        sa = httpd.socket.getsockname()
        msg = "Serving HTTP on", sa[0], "port", sa[1], "..."
        if self.quiet is True:
            print msg
        self.log(msg, 'INFO')
        httpd.serve_forever()

    
def main():
    # Initialize
    diffcollector = PeriodicDiffs()
    try:
        diffcollector.parse_arguments()
        diffcollector.run()
    except:
        diffcollector.exception()
    
if __name__ == '__main__':
    main()

