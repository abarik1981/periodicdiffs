=======
Example
=======

Getting Started - Running within NXOS
=====================================

Requirements
------------
Windows 7 as Client machine with following requirements:

1) Putty to connect to NXOS device and 

2) 7-Zip Manager to compress source code to .tar.gz

3) Tftpd32 to make Windows 7 as Server

A NXOS device as Switch with following requirements:

1) This device must be accessible to client

2) The device must have NXOS API, particularly Python script cli


Tutorial - First Run
--------------------
1) From Client, Download a copy of program from git repo (https://bitbucket.org/abarik1981/periodicdiffs/downloads)

2) Unzip to see the source code

	a) Compress the source code to .tar.gz using 7-Zip in Windows (1. Archive to Tar, 2. Then archive to Gzip)
	
3) Login into NXOS Device and enter the following commands:
 
CLI snippet::

	# Assuming 10.82.254.194 is IP address of laptop
	RCDNDCN9K1# cd bootflash:
	RCDNDCN9K1# mkdir ambarik
	RCDNDCN9K1# copy tftp://10.82.254.194/periodicdiffs.tar.gz bootflash:ambarik/periodicdiffs.tar.gz vrf management
	RCDNDCN9K1# tar extract bootflash:ambarik/periodicdiffs.tar.gz to bootflash:ambarik
	RCDNDCN9K1# run bash
	RCDNDCN9K1:/bootflash/$ cd /bootflash/ambarik
	RCDNDCN9K1:/bootflash/ambarik$ cat > cmds.ini
	show clock
	show ip int br
	# Any other commands
	^C
	RCDNDCN9K1:/bootflash/ambarik$ python /bootflash/ambarik/periodicdiffs/wrapper.py --dir=/bootflash/ambarik/ --cron="* * * * *" --cmdsfile=/bootflash/ambarik/cmds.ini

4) Follow the onscreen messages.

5) To look at the diff reports: cat /bootflash/ambarik/reports/periodicdiffs.txt

6) Enjoy!!



Getting Started - Running outside NXOS
======================================

Requirements
------------
CentOS as External Linux machine with following requirements:

1) Python 2.7 must be installed 

2) Virtualenv must be installed (OPTIONAL)

3) Python lxml package must be installed

CLI snippet::

	# From within virtualenv with python 2.7
	(virtual_env)[admin@localhost ~]$ yum install libxslt-devel libxml2-devel
	(virtual_env)[admin@localhost ~]$ pip install lxml
	
A NXOS device as Switch with following requirements:

1) This device must be accessible to client

2) The device must have nxapi enabled

CLI snippet::

	SWITCH-1# conf
	SWITCH-1(config)# feature nxapi
	SWITCH-1(config)# show nxapi
	nxapi enabled
	Listen on port 80
	Listen on port 443

	
Tutorial - First Run
--------------------
1) From Client, Download a copy of program from git repo (https://bitbucket.org/abarik1981/periodicdiffs/downloads)

2) Unzip to see the source code using unzip utility

	a) This source code already has necessary NXOS API (https://github.com/datacenter/nexus9000/tree/master/nx-os/nxapi/utils)
	
3) Run the program (with external flag set) from client's virtual env bash prompt

CLI snippet::

	(nxos_api)[admin@localhost periodicdiffs]$ python wrapper.py --dir=/tmp/periodicdiffs/ --cron="* * * * *" --cmdsfile=/tmp/periodicdiffs/cmds.ini --external

4) Follow the onscreen messages.

5) To look at the unified diff reports: cat /tmp/periodicdiffs/reports/periodicdiffs.txt

6) To look at the HTML diff report: Navigate to http://<ServerIP>:8008, where ServerIP is the IP address of the machine running the script (PeriodicDiff, by default, starts a HTTP Server at 8008)

	a) Sample HTML report snapshot: ./ExampleHTMLReport.png

7) Enjoy!!

NOTE: target_url must end with "/ins" or else you get HTTP 405 error


Sample Outputs
--------------
This sample program shows that the periodicdiffs is running every 1 minute to save the diff of port-channel 1, fabric stats and traffic stats


CLI snippet from Server::

    # Commands
    (nxos_api)[admin@localhost periodicdiffs]$ cat /tmp/periodicdiffs/cmds.ini 
    show module fabric
    show int port-channel 1 | i "error|packet"
    show ip traffic
    (nxos_api)[admin@localhost periodicdiffs]$ 
    
    # Run the program...
    (nxos_api)[admin@localhost periodicdiffs]$ python wrapper.py --dir=/tmp/periodicdiffs/ --cron="* * * * *" --cmdsfile=/tmp/periodicdiffs/cmds.ini --external -q
    wrapper   : INFO     Logger logs are saved in /tmp/periodicdiffs/logs/periodicdiffs.log
    PeriodicDiff Program started in background!!
            
            
            1. This program will run all the commands found in file /tmp/periodicdiffs/cmds.ini
            2. This program will run these commands periodically as cron values * * * * *
            3. This programm will save the output, e.i diff reports, here: /tmp/periodicdiffs/reports
            4. For debugging or looking at logs, see in here: /tmp/periodicdiffs/logs
            
            Thank you for using this program. For any questions, please contact
            Amit Barik at ambarik@cisco.com
            
            
    
            To kill this program (with PID: 13715), do the following:
                    hostname# run bash
                    hostname$ kill -9 13715
            
    periodicdiffs: INFO     Logger logs are saved in /tmp/periodicdiffs/logs/periodicdiffs.log
    
    
    You are running from external Linux device.
    You will be asked inputs.
    Please fill them correctly.
    NOTE: Because, you are running from External Linux device,
    its expected that NXOS deviceis running nxapi (conf; feature nxpai)
    
    Please provide the URL with format: https://<Mgmt IP>/ins: http://10.201.30.194/ins
    Please provide NXOS device username: CISCO\ambarik
    Please provide NXOS device password: 
    cmdsexe   : INFO     Logger logs are saved in /tmp/periodicdiffs/logs/20140713002333/cmdsexe.log
    
    
    
    # From another console, see the reports
    [admin@localhost periodicdiffs]$ cat /tmp/periodicdiffs/reports/periodicdiffs.txt 
    # Periodicdiff.txt report
    # @author: Amit Barik
    # Created at 2014-07-13 00:23:35.451250 (Timestamps are wrt periodicdiffs Server Timezone)
    # This file contains all the diffs b/w cron-runs reported by periodicdiffs program in plain text format
    # Running with cron-job '* * * * *'
    # The diff in b/w outputs of current vs previous cron-job is shown in Unix diff unified format
    # Output of commands are found in file /tmp/periodicdiffs/logs/%timestamp%/cmds-Out.ini where '%timestamp%' is the current or previous timestamp
    
    
    
    ##########
    ########### Diff b/w CurrentTimestamp=(20140713002333) Vs PreviousTimestamp=(First Run) ##########
    ##########
    
    
    
    ##########
    ########### Diff b/w CurrentTimestamp=(20140713002400) Vs PreviousTimestamp=(20140713002333) ##########
    ##########
    ###### Diff for output of command "show module fabric" #####
    
    ###### Diff for output of command "show int port-channel 1 | i "error|packet"" #####
    --- Command output: 'show int port-channel 1 | i "error|packet"' at     13-Jul-2014 00:24:00 
    +++ Command output: 'show int port-channel 1 | i "error|packet"' at     13-Jul-2014 00:23:33 
    @@ -1,4 +1,4 @@
    -   30 seconds input rate 224 bits/sec, 0 packets/sec
    ?                         ^^
    +   30 seconds input rate 64 bits/sec, 0 packets/sec
    ?                         ^
    -   30 seconds output rate 384 bits/sec, 0 packets/sec
    ?                          ^ ^
    +   30 seconds output rate 288 bits/sec, 0 packets/sec
    ?                          ^ ^
    -     39069 unicast packets  91265 multicast packets  7 broadcast packets
    ?         ^                      ^
    +     39068 unicast packets  91261 multicast packets  7 broadcast packets
    ?         ^                      ^
    -     130341 input packets  60836007 bytes
    ?         ^^                    ^^^^
    +     130336 input packets  60835149 bytes
    ?         ^^                    ^^^^
    @@ -7,2 +7,2 @@
    -     39069 unicast packets  152015 multicast packets  7 broadcast packets
    ?         ^                      ^^
    +     39068 unicast packets  152009 multicast packets  7 broadcast packets
    ?         ^                      ^^
    -     191091 output packets  77846212 bytes
    ?         ^^                     ^^^^
    +     191084 output packets  77844794 bytes
    ?         ^^                     ^^^^
    
    ###### Diff for output of command "show ip traffic" #####
    --- Command output: 'show ip traffic' at        13-Jul-2014 00:24:00 
    +++ Command output: 'show ip traffic' at        13-Jul-2014 00:23:33 
    @@ -5,1 +5,1 @@
    -   Packets received: 3162122, sent: 576289, consumed: 475551,
    ?                         ^^^            ^^                ^^
    +   Packets received: 3162048, sent: 576263, consumed: 475526,
    ?                         ^^^            ^^                ^^
    @@ -20,1 +20,1 @@
    -   Ingress option processing failed: 0  Ingress mforward failed: 2270454  Ingress lisp drop: 0  Ingress Drop (ifmgr init): 0,
    ?                                                                      ^^
    +   Ingress option processing failed: 0  Ingress mforward failed: 2270409  Ingress lisp drop: 0  Ingress Drop (ifmgr init): 0,
    ?                                                                      ^^
    @@ -41,2 +41,2 @@
    -   ICMP originate Req: 304088, Redirects Originate Req: 0
    ?                            ^
    +   ICMP originate Req: 304084, Redirects Originate Req: 0
    ?                            ^
    -   Originate deny - Resource fail: 0, short ip: 0, icmp: 0, others: 304024
    ?                                                                         ^
    +   Originate deny - Resource fail: 0, short ip: 0, icmp: 0, others: 304020
    ?                                                                         ^
    @@ -59,1 +59,1 @@
    -   Pkts recv: 3162053, Bytes recv: 299870126,
    ?                 ^^^^                  ^^^
    +   Pkts recv: 3161979, Bytes recv: 299863260,
    ?                 ^^^^                  ^^  +
    @@ -63,3 +63,3 @@
    -    indiscards: 0, indelivers: 750869,
    ?                                   ^^
    +    indiscards: 0, indelivers: 750840,
    ?                                   ^^
    -    inmcastpkts: 2270454, inmcastbytes: 114451195,
    ?                      ^^                    ^^^ ^
    +    inmcastpkts: 2270409, inmcastbytes: 114448907,
    ?                      ^^                    ^^ ^^
    -    inbcastpkts: 415859,
    ?                      ^
    +    inbcastpkts: 415855,
    ?                      ^
    @@ -67,1 +67,1 @@
    -   outrequests: 582898, outnoroutes: 24398, outforwdgrams: 229,
    ?                    ^^
    +   outrequests: 582872, outnoroutes: 24398, outforwdgrams: 229,
    ?                    ^^
    @@ -69,2 +69,2 @@
    -   outfragfails: 0, outfragcreates: 35515, outtransmits: 576220,
    ?                                                            ^^^
    +   outfragfails: 0, outfragcreates: 35515, outtransmits: 576194,
    ?                                                            ^^^
    -   bytes sent: 414806073, outmcastpkts: 0, outmcastbytes: 0,
    ?                   ^ ---
    +   bytes sent: 414797846, outmcastpkts: 0, outmcastbytes: 0,
    ?                  +++ ^
    [admin@localhost periodicdiffs]$ 
        